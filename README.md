# Distributed Systems Course

## Добро пожаловать!

[Настраиваем рабочее окружение](docs/setup.md)

[Как сдавать задачи](docs/ci.md)

[Приложение](http://130.193.35.33:5001/)

## Библиотеки

* [Await](https://gitlab.com/Lipovsky/await) – Файберы, фьючи и комбинаторы
* [Whirl](https://gitlab.com/Lipovsky/whirl) – Детерминированный симулятор распределенной системы
