# Работа в CLion

## Начальная настройка

### Шаг 0

[Установите CLion](https://www.jetbrains.com/ru-ru/clion/).

### Шаг 1

Курс – это CMake-проект, так что просто откройте его в CLion: `File` > `Open...` > выбрать директорию локального репозитория курса.

### Шаг 2

Настраиваем [Remote development](https://www.jetbrains.com/help/clion/remote-projects-support.html).

В `Preferences` > `Build, Execution, Deployment` > `Toolchains` создайте Remote Host и заполните поля как на скриншоте:

![Setup remote host](images/toolchain.png)

#### 2.1 Credentials

Нажмите на шестеренку в поле `Credentials` и создайте `SSH Configuration` заполняя следующие поля: 

| Поле | Значение |
| - | - |
| **Host** | `127.0.0.1` |
| **Port** | `2222`
| **User name** | `clion_user` |
| **Password** | `password` |

![Setup remote host](images/credentials.png)

#### 2.2 Tools

Заполните поля:

| Поле | Значение |
| - | - |
| **Make** | `/usr/bin/make` |
| **C Compiler** | `/usr/bin/clang-10`
| **C++ Compiler** | `/usr/bin/clang++-10` |

Проверьте, что в поле `Make` вы написали путь именно к `make`, а не к `cmake`.

### Шаг 3

В `Preferences` > `Build, Execution, Deployment` > `CMake` добавьте новый профиль сборки и установите в нем созданный шагом ранее тулчейн:

![Setup remote host](images/profile.png)

### Шаг 4

Готово! Теперь можно выбрать в IDE цель с задачей/тестами и запустить её!

## Полезные советы

- В любой непонятной ситуации с удаленной сборкой следует попробовать следующее заклинание:

ПКМ по корневой папке репозитория > `Deployment` > `Upload to...`

- В окошке `Terminal` можно залогиниться в контейнер и работать там с консольным клиентом `clippy`