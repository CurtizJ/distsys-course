#include <kv/types.hpp>
#include <kv/client/stub.hpp>
#include <kv/node/node.hpp>

// Simulation

#include <whirl/matrix/world/world.hpp>
#include <whirl/matrix/world/global/global.hpp>
#include <whirl/matrix/client/client.hpp>
#include <whirl/matrix/adversary/adversary_base.hpp>
#include <whirl/matrix/history/printers/kv.hpp>
#include <whirl/matrix/history/checker/check.hpp>
#include <whirl/matrix/history/models/kv.hpp>
#include <whirl/matrix/test/reporter.hpp>

#include <whirl/rpc/impl/id.hpp>
#include <await/fibers/core/id.hpp>

#include <random>

using namespace whirl;

//////////////////////////////////////////////////////////////////////

class KVClient : public ClientBase {
 public:
  KVClient(NodeServices services) : ClientBase(std::move(services)) {
  }

 protected:
  void MainThread() override {
    kv::KVBlockingStub kv_store{Channel()};

    for (size_t i = 1;; ++i) {
      if (RandomNumber() % 2 == 0) {
        kv::Key key = "test";
        kv::Value value = RandomNumber(1, 100);
        NODE_LOG("Execute Set({}, {})", key, value);
        kv_store.Set(key, value);
        NODE_LOG("Set completed");
      } else {
        kv::Key key = "test";
        NODE_LOG("Execute Get({})", key);
        kv::Value result = kv_store.Get(key);
        NODE_LOG("Get({}) -> {}", key, result);
        (void)result;
      }

      Threads().SleepFor(RandomNumber(1, 100));
    }
  }
};

//////////////////////////////////////////////////////////////////////

class Adversary : public adversary::AdversaryBase {
 public:
  Adversary(ThreadsRuntime runtime) : AdversaryBase(runtime) {
  }

 protected:
  void Initialize() {
    good_times_ = GlobalRandomNumber(1000, 4000);
  }

  void AbuseNetwork(IFaultyNetwork& net) {
    while (true) {
      if (NoMoreFaults()) {
        break;
      }

      RandomPause(50, 1000);
      net.Split();
      RandomPause(50, 1000);
      net.Heal();
    }
  }

  void AbuseServer(IFaultyServer& server) {
    while (true) {
      if (NoMoreFaults()) {
        break;
      }

      RandomPause(300, 1000);
      if (GlobalRandomNumber() % 3 == 0) {
        if (server.IsAlive()) {
          server.FastReboot();
        }
      } else if (GlobalRandomNumber() % 5 == 0) {
        server.AdjustWallClock();
      }
    }
  }

  void AbuseWholeCluster(std::vector<IFaultyServer*> cluster) {

    size_t crash_budget = CrashesBudget();
    size_t size = cluster.size();

    while (true) {
      RandomPause(50, 700);

      if (GlobalRandomNumber() % 2 == 0) {
        if (crash_budget > 0) {
          crash_budget--;

          // Choose victim
          size_t victim = GlobalRandomNumber(size);

          std::swap(cluster[victim], cluster[size - 1]);
          size--;

          // Crash victim
          WHIRL_FMT_LOG("Explode server {}", cluster[size]->Name());
          cluster[size]->Crash();

          if (crash_budget == 0) {
            break;
          }
        }
      }
    }
  }

 private:
  size_t CrashesBudget() const {
    return (GetClusterSize() - 1) / 2;
  }

  bool NoMoreFaults() const {
    return GlobalNow() > good_times_;
  }

 private:
  TimePoint good_times_;
};

//////////////////////////////////////////////////////////////////////

using KVStoreModel = histories::KVStoreModel<kv::Key, kv::Value>;

//////////////////////////////////////////////////////////////////////

TestReporter reporter;

//////////////////////////////////////////////////////////////////////

bool TooBigToCheck(const histories::History& history) {
  return histories::Cleanup<KVStoreModel>(history).size() >= 15;
}

//////////////////////////////////////////////////////////////////////

// [3, 7]
size_t NumberOfReplicas(size_t seed) {
  return 3 + seed % 3;
}

// Seed -> simulation digest
size_t RunSimulation(size_t seed) {
  static const size_t kTimeLimit = 30000;
  static const size_t kCompletedCalls = 5;

  reporter.PrintSimSeed(seed);

  // Reset RPC and fiber ids
  await::fibers::ResetIds();
  whirl::rpc::ResetIds();

  World world{seed};

  // Cluster nodes
  auto node = MakeNode<kv::KVNode>();
  world.AddServers(NumberOfReplicas(seed), node);

  // Clients
  auto client = MakeNode<KVClient>();
  world.AddClients(2, client);

  // Adversary
  world.SetAdversary(
      adversary::MakeStrategy<Adversary>());

  // Log
  std::stringstream log;
  world.WriteLogTo(log);

  // Run simulation
  world.Start();

  while (world.NumCompletedCalls() < kCompletedCalls &&
      world.TimeElapsed() < kTimeLimit) {
    world.Step();
  }

  // Stop and compute simulation digest
  size_t digest = world.Stop();

  // Print report
  reporter.PrintSimReport(world);

  // Time limit exceeded
  if (world.NumCompletedCalls() < kCompletedCalls) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    reporter.PrintSimLog(log.str());
    reporter.Fail("Simulation time limit exceeded");
  }

  const auto history = world.History();
  reporter.DebugLine("History size: {}", history.size());

  if (TooBigToCheck(history)) {
    reporter.DebugLine("Skip check, history is too big");
    return digest;  // Too long to test...
  }

  const bool linearizable = histories::LinCheck<KVStoreModel>(history);

  if (!linearizable) {
    reporter.PrintLine("Simulation {} FAILED", reporter.SimIndex());
    reporter.PrintSimLog(log.str());
    reporter.PrintLine("History (seed = {}) is NOT LINEARIZABLE:", seed);
    reporter.PrintSimHistory<KVStoreModel>(history);
    reporter.Fail();
  }

  return digest;
}

//////////////////////////////////////////////////////////////////////

void RunSimulations(size_t count) {
  std::mt19937 seeds{42};

  reporter.PrintLine("Run simulations...");

  for (size_t i = 1; i <= count; ++i) {
    reporter.StartSim();
    RunSimulation(seeds());
    reporter.CompleteSim();
  }
}

//////////////////////////////////////////////////////////////////////

int main() {
  RunSimulations(22222);

  reporter.Congratulate();

  return 0;
}
