#pragma once

#include <paxos/node/proposal.hpp>
#include <paxos/node/protocol.hpp>

#include <whirl/node/logging.hpp>
#include <whirl/node/peer_base.hpp>
#include <whirl/rpc/use/service_base.hpp>

namespace paxos {

using whirl::NodeServices;
using whirl::PeerBase;
using whirl::rpc::ServiceBase;

////////////////////////////////////////////////////////////////////////////////

// Acceptor role / RPC service

class Acceptor : public ServiceBase<Acceptor>, public PeerBase {
 public:
  Acceptor(NodeServices services) : PeerBase(std::move(services)) {
  }

 protected:
  void RegisterRPCMethods() {
    // Your code goes here
  }

};

}  // namespace paxos
