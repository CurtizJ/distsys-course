# KV + Multi-Paxos = ♥

## Пролог

_There are only two hard problems in distributed systems:  2. Exactly-once delivery 1. Guaranteed order of messages 2. Exactly-once delivery_

[Источник](https://twitter.com/mathiasverraes/status/632260618599403520)

## Задача

Реализуйте отказоустойчивое линеаризуемое KV хранилище.

## Сервисы

Система должна предоставлять клиентам RPC-сервис `KV` с методами:

| Сигнатура | Семантика |
| - | - |
| `void Set(Key key, Value value)` | Записать значение `value` по ключу `key` |
| `Value Get(Key key)` | Прочесть значение по ключу `key` |
| `Value Cas(Key key, Value expected, Value target)` | _Атомарно_: 1) прочесть значение по ключу `key` 2) если прочитанное значение совпало с `expected`, то записать по этому ключу значение `target` 3) вернуть прочитанное на первом шаге значение

[Код заменит тысячу слов](./rsm/kv/store/store.hpp)

## Отказоустойчивость

Система должна переживать:

- Рестарты узлов
- Партишены сети
- Отказ произвольного меньшинства узлов

## Модель согласованности

[Линеаризуемость](https://jepsen.io/consistency/models/linearizable)

## RSM

![RSM и Primary-Backup](./images/rsm.png)

## Multi-Paxos

Используйте алгоритм _Multi-Paxos_

- [Paxos lecture](https://www.youtube.com/watch?v=JEpsBg0AO6o) by Diego Ongaro, John Ousterhout (авторы RAFT), [слайды](https://ongardie.net/static/raft/userstudy/paxos.pdf)
- [Part-Time Parliament](https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf)

## Exactly-once

Ретраи RPC-запросов от клиентов к узлам системы не должны проводить к повторному применению мутирующих команд (`Set` и `Cas`).

Cм. п. 6.3 из [RAFT PhD](https://web.stanford.edu/~ouster/cgi-bin/papers/OngaroPhD.pdf)

## Шаблон решения

[Вот он!](rsm)

## Как читать код

Шаблон решения состоит из двух частей: `multipaxos` и `kv`.

* [`multipaxos`](./rsm/multipaxos) - фреймворк RSM (_Replicated State Machine_), использующий алгоритм _Multi-Paxos_ для репликации лога команд
* [`kv`](./rsm/kv) – реплицированное KV хранилище, реализованное с помощью фреймворка `multipaxos`

### RSM

#### Составные части

* [`Command`](./rsm/multipaxos/command.hpp) – сериализованная команда
* `CommandOutput` – сериализованный результат применения команды к автомату (например, прочитанное значение для команды `Get`)
* [`IStateMachine`](./rsm/multipaxos/state_machine.hpp) – реплицируемый автомат, умеет применять команды и возвращать результаты:

```cpp
// Применяем команду, получаем результат
Result<CommandOutput> result = state_machine->Apply(command);
```

* [`LogEntry`](./rsm/multipaxos/log_entry.hpp) – запись в логе команд
* [`PersistentLog`](./rsm/multipaxos/log.hpp) – персистентный лог

Наконец, [`MultiPaxos`](./rsm/multipaxos/multipaxos.cpp) – реализация интерфейса [`IReplicatedStateMachine`](./rsm/multipaxos/rsm.hpp):

```cpp
// Создаем RMS, реплицирующую `kv_state_machine`
auto rsm = MakeMultiPaxosRSM("kv", kv_state_machine, runtime);
```

```cpp
// Бросаем команду `command` в конвейер алгоритма Multi-Paxos
Future<CommandOutput> future = rsm->Execute(command);
```

Слой RSM ничего не знает про устройство реплицируемого автомата и семантику применяемых к нему команд.

#### KV

* `client` – клиентская библиотека 
* `store` – реплицируемое состояние – [TinyKVStore](./rsm/kv/store/store.hpp)
* `node` – реплика RSM
* `proto` – запросы, которые клиент отправляет реплике

Знание про типы и семантику команд сосредоточены в
* [`kv::ClientService`](./rsm/kv/client_service.hpp)
* [`kv::StateMachine`](./rsm/kv/node/state_machine.cpp)

Чтобы разобраться в шаблоне, попробуйте отследить путь исполнения команды от вызова метода `kv::BlockingClient` до получения ответа от `kv::Node`.

## Чего от меня хотят?

Реализовать метод [`MultiPaxos::Execute`](rsm/multipaxos/multipaxos.cpp).

По пути у вас возникнет желание доработать и другие места фреймворка RSM.

Часть таких мест отмечена в коде комментариями `// TODO`.

## Указания по реализации

Продолжение следует...
