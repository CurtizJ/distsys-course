#pragma once

#include <rsm/kv/store/store.hpp>
#include <rsm/kv/proto/proto.hpp>

#include <whirl/matrix/history/history.hpp>
#include <whirl/matrix/history/printers/print.hpp>

#include <wheels/support/panic.hpp>

namespace kv {

using whirl::histories::Call;
using whirl::histories::History;

using Input = whirl::histories::Arguments;
using Output = whirl::histories::Value;

//////////////////////////////////////////////////////////////////////

struct Printer {
  static std::string Print(const Call& call) {
    std::stringstream out;

    if (call.method == "Set") {
      PrintSet(call, out);
    } else if (call.method == "Get") {
      PrintGet(call, out);
    } else if (call.method == "Cas") {
      PrintCas(call, out);
    }

    return out.str();
  }

  static void PrintSet(const Call& call, std::ostream& out) {
    // Name
    out << call.method;

    // Arguments
    auto [set] = call.arguments.As<SetRequest>();
    out << "(" << set.key << ", " << set.value << ")";

    // Return value
    if (call.IsCompleted()) {
      // out << ": void";
    } else {
      out << "?";
    }
  }

  static void PrintGet(const Call& call, std::ostream& out) {
    out << call.method;

    // Arguments
    auto [get] = call.arguments.As<GetRequest>();
    out << "(" << get.key << ")";

    // Return value
    if (call.IsCompleted()) {
      out << ": " << call.result.As<Value>();
    } else {
      out << "?";
    }
  }

  static void PrintCas(const Call& call, std::ostream& out) {
    out << call.method;

    // Arguments
    auto [cas] = call.arguments.As<CasRequest>();
    out << "(" << cas.key << ", " << cas.expected_value << ", "
        << cas.target_value << ")";

    // Return value
    if (call.IsCompleted()) {
      Value old_value = call.result.As<Value>();
      out << ": " << old_value;
    } else {
      out << "?";
    }
  }
};

//////////////////////////////////////////////////////////////////////

class Model {
 public:
  using State = TinyKVStore;

  static State InitialState() {
    return {};
  }

  struct Result {
    bool ok;
    Output value;
    State next_state;
  };

  static Result Apply(const State& current, const std::string& method,
                      const Input& input) {
    if (method == "Set") {
      // Set
      auto [set] = input.As<SetRequest>();
      State next{current};
      next.Set(set.key, set.value);
      return {true, Output::Void(), std::move(next)};

    } else if (method == "Get") {
      // Get
      auto [get] = input.As<GetRequest>();
      kv::Value value = current.Get(get.key);
      return {true, Output::Make(value), current};

    } else if (method == "Cas") {
      // Cas aka Compare-And-Set
      auto [cas] = input.As<CasRequest>();
      State next{current};
      kv::Value old_value = next.Cas(cas.key, cas.expected_value, cas.target_value);
      return {true, Output::Make(old_value), std::move(next)};
    }

    WHEELS_PANIC("Method not supported by KV model: '" << method << "'");
  }

  static bool IsMutation(const Call& call) {
    return call.method == "Set" || call.method == "Cas";
  }

  static bool IsReadOnly(const Call& call) {
    return call.method == "Get";
  }

  static std::vector<History> Decompose(const History& history) {
    // TODO: split by key
    return {history};
  }

  static std::string Print(State state) {
    std::stringstream out;
    out << "{";
    for (const auto& [k, v] : state) {
      out << k << " -> " << v << ", ";
    }
    out << "}";
    return out.str();
  }

  using CallPrinter = Printer;

  static std::string PrintCall(const Call& call) {
    return CallPrinter::Print(call);
  }
};

}  // namespace kv
