#include <rsm/kv/proto/proto.hpp>

namespace kv {

std::ostream& operator<<(std::ostream& out, const SetRequest& set) {
  out << "SetRequest(" << set.key << ", " << set.value << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const GetRequest& get) {
  out << "GetRequest(" << get.key << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const CasRequest& cas) {
  out << "CasRequest(" << cas.key << ", " << cas.expected_value << ", "
      << cas.target_value << ")";
  return out;
}

}  // namespace kv