#include <rsm/kv/client/client.hpp>
#include <rsm/kv/node/node.hpp>
#include <rsm/kv/store/types.hpp>
#include <rsm/kv/model/model.hpp>

#include <whirl/node/node_base.hpp>
#include <whirl/node/logging.hpp>

// Simulation
#include <whirl/matrix/world/world.hpp>
#include <whirl/matrix/client/client.hpp>
#include <whirl/matrix/history/checker/check.hpp>
#include <whirl/matrix/world/global/vars.hpp>

#include <whirl/matrix/test/random.hpp>
#include <whirl/matrix/test/reporter.hpp>

#include <await/fibers/core/id.hpp>
#include <whirl/rpc/impl/id.hpp>

// Concurrency
#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>

#include <cereal/types/string.hpp>
#include <fmt/ostream.h>

#include <random>

using namespace await::fibers;
using namespace whirl;

//////////////////////////////////////////////////////////////////////

class KVClient final : public ClientBase {
 public:
  KVClient(NodeServices runtime) : ClientBase(std::move(runtime)) {
  }

 protected:
  void MainThread() override {
    kv::BlockingClient kv_client{Channel(), GenerateUid()};

    const size_t requests_to_do = GetGlobal<size_t>("requests_per_client");

    for (size_t i = 0; i < requests_to_do; ++i) {
      if (RandomNumber() % 2 == 0) {
        kv::Key key = ChooseKey();
        kv::Value value = ToValue(RandomNumber(1, 100));
        NODE_LOG("Execute Set({}, {})", key, value);
        kv_client.Set(key, value);
        NODE_LOG("Set completed");
      } else {
        kv::Key key = ChooseKey();
        NODE_LOG("Execute Get({})", key);
        kv::Value result = kv_client.Get(key);
        NODE_LOG("Get({}) -> {}", key, result);
      }

      GlobalCounter("requests_done").Increment();

      // Sleep for some time
      Threads().SleepFor(RandomNumber(1, 100));
    }
  }

 private:
  static kv::Value ToValue(size_t integer) {
    return std::to_string(integer);
  }

  kv::Key ChooseKey() {
    return "test";
  }
};

//////////////////////////////////////////////////////////////////////

TestReporter reporter;

//////////////////////////////////////////////////////////////////////

// Seed -> simulation digest
size_t RunSimulation(size_t seed) {
  static const size_t kTimeLimit = 20000;

  reporter.PrintSimSeed(seed);

  Random random{seed};

  // Simulation parameters

  const size_t replicas = random.Get(3, 5);
  const size_t clients = random.Get(3, 4);
  const size_t requests_per_client = random.Get(3, 4);

  const size_t total_requests = clients * requests_per_client;

  reporter.DebugLine(
      "Parameters: replicas = {}, clients = {}, requests_per_client = {}",
      replicas, clients, requests_per_client);

  // Reset RPC and fiber ids
  await::fibers::ResetIds();
  whirl::rpc::ResetIds();

  World world{seed};

  // Globals
  world.SetGlobal("requests_per_client", requests_per_client);
  world.InitCounter("requests_done");

  // Cluster nodes
  auto node = MakeNode<kv::Node>();
  world.AddServers(replicas, node);

  // Clients
  auto client = MakeNode<KVClient>();
  world.AddClients(clients, client);

  // Log
  std::stringstream log;
  world.WriteLogTo(log);

  // Run simulation
  world.Start();
  while (world.GetCounter("requests_done") < total_requests &&
      world.TimeElapsed() < kTimeLimit) {
    if (!world.Step()) {
      break;
    }
  }

  // Stop and compute simulation digest
  size_t digest = world.Stop();

  // Print report
  reporter.PrintSimReport(world);

  // Time limit exceeded
  if (world.GetCounter("requests_done") < total_requests) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    reporter.PrintSimLog(log.str());

    if (world.TimeElapsed() >= kTimeLimit) {
      reporter.Fail("Simulation time limit exceeded");
    } else {
      reporter.Fail("Simulation hanged");
    }
  }

  // Check linearizability
  const auto history = world.History();

  reporter.DebugLine("History size: {}", history.size());

  const bool linearizable = histories::LinCheck<kv::Model>(history);

  if (!linearizable) {
    reporter.PrintLine("Simulation {} (seed = {}) FAILED", reporter.SimIndex(), seed);
    // Log
    reporter.PrintSimLog(log.str());
    // History
    reporter.PrintLine("History (seed = {}) is NOT LINEARIZABLE:", seed);
    reporter.PrintSimHistory<kv::Model>(history);
    reporter.Fail();
  }

  return digest;
}

void TestDeterminism() {
  static const size_t kSeed = 104107713;

  reporter.PrintLine("Test determinism...");

  size_t digest = RunSimulation(kSeed);

  // Repeat with the same seed
  if (RunSimulation(kSeed) != digest) {
    reporter.Fail("Impl is not deterministic");
  } else {
    reporter.PrintLine("Ok");
  }
}

void RunSimulations(size_t count) {
  std::mt19937 seeds{42};

  reporter.ResetSimCount();
  reporter.PrintLine("Run simulations...");

  for (size_t i = 1; i <= count; ++i) {
    reporter.StartSim();
    RunSimulation(seeds());
    reporter.CompleteSim();
  }
}

int main() {
  TestDeterminism();
  RunSimulations(12345);

  reporter.Congratulate();

  return 0;
}
